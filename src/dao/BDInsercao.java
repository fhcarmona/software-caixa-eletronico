package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BDInsercao
{
	// EXECUTA UMA QUERY DE SELECAO
	public void insert( String query, Connection conexao ) 
	{
		try 
		{
			// RETORNA O RESULTADO DA QUERY			
			conexao.prepareStatement( query ).executeUpdate();
		} 
		catch( Exception erro ) 
		{ 
			erro.printStackTrace(); 
			
			try 
			{ 
				conexao.rollback(); 
			} 
			catch( SQLException erroRollBack ) 
			{ 
				System.out.print( erroRollBack.getStackTrace() ); 
			} 
		}
	}
}
