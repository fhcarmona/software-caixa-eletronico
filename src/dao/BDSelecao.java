package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class BDSelecao
{
	// EXECUTA UMA QUERY DE SELECAO
	public ResultSet select( String query, Connection conexao ) 
	{
		try 
		{
			// RETORNA O RESULTADO DA QUERY			
			return conexao.prepareStatement( query ).executeQuery();
		} 
		catch( Exception erro ) 
		{ 
			erro.printStackTrace(); 
			
			try 
			{ 
				conexao.rollback(); 
			} 
			catch( SQLException erroRollBack ) 
			{ 
				System.out.print( erroRollBack.getStackTrace() ); 
			} 
		}
		
		return null;
	}
}