package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BDConexao
{
	static
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
		}
		catch( ClassNotFoundException erro )
		{
			throw new RuntimeException( erro );
		}
	} 
	
	public Connection novaConexao() throws SQLException
	{
		return DriverManager.getConnection( "jdbc:mysql://localhost/caixa-eletronico?user=root&password=" );
	}
} 