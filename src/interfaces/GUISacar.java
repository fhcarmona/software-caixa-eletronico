// PACOTE
package interfaces;

// BIBLIOTECAS
import java.util.ResourceBundle;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import logica.Conta;


// CLASSE PRINCIPAL
public class GUISacar extends JFrame implements ActionListener
{		
	// ATRIBUTOS
	private GridBagLayout janela;
	
	private JButton btnRetirarDez;
	private JButton btnRetirarVinte;
	private JButton btnRetirarCinquenta;
	private JButton btnRetirarCem;
	private JButton btnRetirarOutro;
	private JButton btnCancelar;
	
	private JLabel  lblNome;
	
	private ResourceBundle linguagem;
	
	private Conta conta = Conta.getInstancia();

	public GUISacar()
	{			
		// LINGUAGEM E DADOS DO USUARIO ATUAL
		this.linguagem = conta.getLinguagem();
		
		// JANELA
		janela = new GridBagLayout();
        setLayout( janela );
        setTitle( linguagem.getString( "guiSacar.titulo" ) );
        
        // ATRIBUTOS DO LABEL
        lblNome = new JLabel( conta.getUsuario().getNome() );
        lblNome.setPreferredSize( new Dimension( 300, 75 ) );
        lblNome.setHorizontalAlignment( SwingConstants.CENTER );
        
        // NOME DOS BOTOES
        btnRetirarDez		= new JButton( linguagem.getString( "guiSacar.botaoRetirarDez"		 ) );
        btnRetirarVinte		= new JButton( linguagem.getString( "guiSacar.botaoRetirarVinte"	 ) );
        btnRetirarCinquenta	= new JButton( linguagem.getString( "guiSacar.botaoRetirarCinquenta" ) );
        btnRetirarCem		= new JButton( linguagem.getString( "guiSacar.botaoRetirarCem"		 ) );
        btnRetirarOutro		= new JButton( linguagem.getString( "guiSacar.botaoRetirarOutro"	 ) );
        btnCancelar			= new JButton( linguagem.getString( "guiSacar.botaoCancelar"		 ) );
        
        // DIMENSAO DE CADA BOTAO
        btnRetirarDez.setPreferredSize		( new Dimension( 300, 75 ) );
        btnRetirarVinte.setPreferredSize	( new Dimension( 300, 75 ) );
        btnRetirarCinquenta.setPreferredSize( new Dimension( 300, 75 ) );
        btnRetirarCem.setPreferredSize		( new Dimension( 300, 75 ) );
        btnRetirarOutro.setPreferredSize	( new Dimension( 300, 75 ) );
        btnCancelar.setPreferredSize		( new Dimension( 300, 75 ) );
        
        // GRIDBAGCONSTRAINTS
        GridBagConstraints gbc1 = new GridBagConstraints();
        GridBagConstraints gbc2 = new GridBagConstraints();
        GridBagConstraints gbc3 = new GridBagConstraints();
        GridBagConstraints gbc4 = new GridBagConstraints();
        GridBagConstraints gbc5 = new GridBagConstraints();
        GridBagConstraints gbc6 = new GridBagConstraints();
        GridBagConstraints gbc7 = new GridBagConstraints();
        
        // ORGANIZACAO DE CADA BOTAO
        gbc1.gridx	   = 0;
        gbc1.gridy	   = 0;    
        gbc1.gridwidth = 2;        
        
        gbc2.gridx  = 0;
        gbc2.gridy  = 1;  
        gbc2.insets = new Insets( 0, 0, 5, 5 );
        
        gbc3.gridx  = 1;
        gbc3.gridy  = 1;
        gbc3.insets = new Insets( 0, 0, 5, 0 );
        
        gbc4.gridx  = 0;
        gbc4.gridy  = 2;
        gbc4.insets = new Insets( 0, 0, 5, 5 );
        
        gbc5.gridx  = 1;
        gbc5.gridy  = 2;
        gbc5.insets = new Insets( 0, 0, 5, 0 );
        
        gbc6.gridx  = 0;
        gbc6.gridy  = 3; 
        gbc6.insets = new Insets( 0, 0, 0, 5 );
        
        gbc7.gridx  = 1;
        gbc7.gridy  = 3;
       
        // ADICIONA CADA BOTAO
        add( lblNome,			  gbc1 );
        add( btnRetirarDez,		  gbc2 );
        add( btnRetirarVinte,	  gbc3 );
        add( btnRetirarCinquenta, gbc4 );
        add( btnRetirarCem,		  gbc5 );
        add( btnRetirarOutro,	  gbc6 );
        add( btnCancelar,		  gbc7 );
        
        // ADICIONA FUNCAO A CADA BOTAO
        btnRetirarDez.addActionListener		 ( this );
        btnRetirarVinte.addActionListener	 ( this );
        btnRetirarCinquenta.addActionListener( this );
        btnRetirarCem.addActionListener		 ( this );
        btnRetirarOutro.addActionListener	 ( this );
        btnCancelar.addActionListener		 ( this );
	}

	@Override
	public void actionPerformed( ActionEvent evento )
	{
		if( evento.getSource() == btnRetirarDez )
			chamarGUIToken( 10 );
		else if( evento.getSource() == btnRetirarVinte )
			chamarGUIToken( 20 );
		else if( evento.getSource() == btnRetirarCinquenta )
			chamarGUIToken( 50 );
		else if( evento.getSource() == btnRetirarCem )
			chamarGUIToken( 100 );
		else if( evento.getSource() == btnRetirarOutro )
			chamarGUISacarEspecifico();
		
		dispose();
	}
	
	public void chamarGUIToken( int valor )
	{
		GUIToken objToken = new GUIToken( valor );
		
		objToken.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );			
		objToken.setSize				 ( 800, 600 );
		objToken.setResizable			 ( false );			
		objToken.setVisible				 ( true );
		objToken.setLocationRelativeTo	 ( null );
	}
	
	public void chamarGUISacarEspecifico()
	{
		GUISacarEspecifico objSacarEspecifico = new GUISacarEspecifico();
		
		objSacarEspecifico.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );			
		objSacarEspecifico.setSize				   ( 800, 600 );
		objSacarEspecifico.setResizable			   ( false );			
		objSacarEspecifico.setVisible			   ( true );
		objSacarEspecifico.setLocationRelativeTo   ( null );
	}
}