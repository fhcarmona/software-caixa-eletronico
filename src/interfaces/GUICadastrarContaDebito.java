// PACOTE
package interfaces;

// BIBLIOTECAS
import java.text.ParseException;
import java.util.ResourceBundle;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.text.MaskFormatter;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import logica.Conta;


// CLASSE PRINCIPAL
public class GUICadastrarContaDebito extends JFrame implements ActionListener
{		
	// ATRIBUTOS
	private GridBagLayout janela;
		
	private JLabel lblNome;
	private JLabel lblBoleto;
	
	private JTextField txtBoleto;
	
	private JButton	btnConfirmar;
	private JButton btnCancelar;
	
	private ResourceBundle linguagem;
	
	private Conta conta = Conta.getInstancia();
	
	public GUICadastrarContaDebito()
	{			
		// FONTE
		Font sansSerif = new Font( "SansSerif", Font.PLAIN, 16 );
		
		// LINGUAGEM
		this.linguagem = conta.getLinguagem();
		
		// JANELA
		janela = new GridBagLayout();
        setLayout( janela );
        setTitle( linguagem.getString( "guiCadastrarContaDebito.titulo" ) );
        
        // INSTANCIA OS CAMPOS
        lblNome		 = new JLabel( conta.getUsuario().getNome() );
        lblBoleto	 = new JLabel( linguagem.getString( "guiCadastrarContaDebito.textoBoleto" ) );
        
        btnConfirmar = new JButton( linguagem.getString( "guiCadastrarContaDebito.botaoConfirmar" ) );
        btnCancelar  = new JButton( linguagem.getString( "guiCadastrarContaDebito.botaoCancelar"  ) );
        
        txtBoleto	 = new JTextField( 23 );
        
        try							 { txtBoleto = new JFormattedTextField( new MaskFormatter( "#####" ) ); }
        catch( ParseException error ){ error.printStackTrace();												}
        
        // ATRIBUI LOCALIDADE A CADA ETIQUETA
        lblNome.setHorizontalAlignment	 ( SwingConstants.CENTER ); 
        lblBoleto.setVerticalAlignment	 ( SwingConstants.TOP	 ); 
        
        // DEFINE O TEXTO A SER ESCRITO A DIREITA
        txtBoleto.setHorizontalAlignment( SwingConstants.RIGHT  );   
        
        // DEFINE O TAMANHO DE CADA CAMPO
        lblNome.setPreferredSize	 ( new Dimension( 300, 75 ) );
        lblBoleto.setPreferredSize	 ( new Dimension( 300, 75 ) );
         
        txtBoleto.setPreferredSize	 ( new Dimension( 300, 45 ) );
               
        btnConfirmar.setPreferredSize( new Dimension( 145, 75 ) );
        btnCancelar.setPreferredSize ( new Dimension( 150, 75 ) );
        
        // ATRIBUTOS DO TEXTO DE CADA CAIXA
        txtBoleto.setFont( sansSerif );
        
        // GRIDBAGCONSTRAINTS
        GridBagConstraints gbc1 = new GridBagConstraints();
        GridBagConstraints gbc2 = new GridBagConstraints();
        GridBagConstraints gbc3 = new GridBagConstraints();
        GridBagConstraints gbc4 = new GridBagConstraints();
        
        // ORGANIZACAO DE CADA BOTAO
        gbc1.gridx	= 0;
        gbc1.gridy	= 0; 
        gbc1.gridwidth = 2;
        
        gbc2.gridx  = 0;
        gbc2.gridy  = 1;  
        gbc2.gridwidth = 2;
        
        gbc3.gridx  = 0;
        gbc3.gridy  = 2;
        gbc3.insets = new Insets( 0, 0, 0, 5 );
        
        gbc4.gridx  = 1;
        gbc4.gridy  = 2;
       
        // ADICIONA CADA CAMPO
        add( lblNome,	   gbc1 );
        add( lblBoleto,    gbc2 );
        add( txtBoleto,    gbc2 ); 
        add( btnConfirmar, gbc3 ); 
        add( btnCancelar,  gbc4 );
        
        // ADICIONA FUNCAO A CADA BOTAO
        btnConfirmar.addActionListener( this );
        btnCancelar.addActionListener ( this );
	}

	@Override
	public void actionPerformed( ActionEvent evento )
	{
		if( evento.getSource() == btnConfirmar )
			JOptionPane.showMessageDialog( null, linguagem.getString( "guiCadastrarContaDebito.erroTexto"  ), linguagem.getString( "guiCadastrarContaDebito.erroTitulo"  ), JOptionPane.WARNING_MESSAGE );
		else if( evento.getSource() == btnCancelar )
			dispose();
	}
}