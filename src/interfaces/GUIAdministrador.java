// PACOTE
package interfaces;

// BIBLIOTECAS
import java.util.ResourceBundle;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import logica.Conta;

// CLASSE PRINCIPAL
public class GUIAdministrador extends JFrame implements ActionListener
{		
	// ATRIBUTOS
	private GridBagLayout janela;
	
	private JButton btnRelatorioOperacoes;
	private JButton btnRelatorioDispenser;
	private JButton btnCancelar;
	
	private JLabel lblNome;
	
	private ResourceBundle linguagem;
	
	private Conta conta = Conta.getInstancia();
	
	public GUIAdministrador()
	{
		this.linguagem = conta.getLinguagem();
		
		janela = new GridBagLayout();
        setLayout( janela );
        setTitle( linguagem.getString( "guiAdministrador.titulo" ) );
        
        // ATRIBUTOS DO LABEL
        lblNome = new JLabel( conta.getUsuario().getNome() );
        lblNome.setPreferredSize( new Dimension( 300, 75 ) );
        lblNome.setHorizontalAlignment( SwingConstants.CENTER );  
        
        // NOME DOS BOTOES
        btnRelatorioOperacoes = new JButton( linguagem.getString( "guiAdministrador.botaoOperacoes"	 ) );
        btnRelatorioDispenser = new JButton( linguagem.getString( "guiAdministrador.botaoDispenser"	 ) );
        btnCancelar			  = new JButton( linguagem.getString( "guiAdministrador.botaoCancelar"	 ) );
        
        // DIMENSAO DE CADA BOTAO
        btnRelatorioOperacoes.setPreferredSize( new Dimension( 300, 75 ) );
        btnRelatorioDispenser.setPreferredSize( new Dimension( 300, 75 ) );
        btnCancelar.setPreferredSize		  ( new Dimension( 300, 75 ) );
        
        // GRIDBAGCONSTRAINTS
        GridBagConstraints gbc1 = new GridBagConstraints();
        GridBagConstraints gbc2 = new GridBagConstraints();
        GridBagConstraints gbc3 = new GridBagConstraints();
        GridBagConstraints gbc4 = new GridBagConstraints();
        
        // ORGANIZACAO DE CADA BOTAO
        gbc1.gridx     = 0;
        gbc1.gridy 	   = 0; 
        
        gbc2.gridx = 0;
        gbc2.gridy = 1; 
        gbc2.insets = new Insets( 0, 0, 5, 0 );
        
        gbc3.gridx = 0;
        gbc3.gridy = 2;
        gbc3.insets = new Insets( 0, 0, 5, 0 );
        
        gbc4.gridx = 0;
        gbc4.gridy = 3;
        gbc4.insets = new Insets( 0, 0, 5, 0 );
       
        // ADICIONA CADA BOTAO
        add( lblNome,				gbc1 );
        add( btnRelatorioOperacoes,	gbc2 );
        add( btnRelatorioDispenser,	gbc3 );
        add( btnCancelar, 			gbc4 );
        
        // ADICIONA FUNCAO A CADA BOTAO
        btnRelatorioOperacoes.addActionListener( this );
        btnRelatorioDispenser.addActionListener( this );
        btnCancelar.addActionListener( this );
	}

	@Override
	public void actionPerformed( ActionEvent evento )
	{
		if( evento.getSource() == btnRelatorioOperacoes )
			chamarGUIRelatorio( 3 );
		else if( evento.getSource() == btnRelatorioDispenser )
			chamarGUIRelatorio( 4 );
		else if( evento.getSource() == btnCancelar )
			dispose();
	}
		
	public void chamarGUIRelatorio( int tipo )
	{
		GUIRelatorios objRelatorio = new GUIRelatorios( tipo );
		
		objRelatorio.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );			
		objRelatorio.setSize				 ( 800, 600 );
		objRelatorio.setResizable			 ( false );			
		objRelatorio.setVisible			  	 ( true );
		objRelatorio.setLocationRelativeTo	 ( null );
	}
}