// PACOTE
package interfaces;

// BIBLIOTECAS
import java.util.ResourceBundle;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import logica.Conta;


// CLASSE PRINCIPAL
public class GUITransferencia extends JFrame implements ActionListener
{		
	// ATRIBUTOS
	private GridBagLayout janela;
		
	private JLabel lblNome;
	private JLabel lblAgencia;
	private JLabel lblConta;
	private JLabel lblValor;
	
	private JTextField txtAgencia;
	private JTextField txtConta;
	private JTextField txtValor;
	
	private JButton	btnConfirmar;
	private JButton btnCancelar;
	
	private ResourceBundle linguagem;
	
	private Conta conta = Conta.getInstancia();
	
	public GUITransferencia()
	{			
		Font sansSerif = new Font( "SansSerif", Font.PLAIN, 16 );
		
		this.linguagem = conta.getLinguagem();
		
		janela = new GridBagLayout();
        setLayout( janela );
        setTitle( linguagem.getString( "guiTransferencia.titulo" ) );
        
        // INSTANCIA OS CAMPOS
        lblNome		 = new JLabel( conta.getUsuario().getNome() );
        lblAgencia	 = new JLabel( linguagem.getString( "guiTransferencia.textoAgencia" ) );
        lblConta	 = new JLabel( linguagem.getString( "guiTransferencia.textoConta"	  ) );
        lblValor	 = new JLabel( linguagem.getString( "guiTransferencia.textoValor"	  ) );
        
        btnConfirmar = new JButton( linguagem.getString( "guiTransferencia.botaoConfirmar" ) );
        btnCancelar  = new JButton( linguagem.getString( "guiTransferencia.botaoCancelar"  ) );
        
        txtAgencia	 = new JTextField( 23 );
        txtConta	 = new JTextField( 23 );
        txtValor	 = new JTextField( 23 );
        
        // ATRIBUI LOCALIDADE A CADA ETIQUETA
        lblNome.setHorizontalAlignment	 ( SwingConstants.CENTER ); 
        lblAgencia.setVerticalAlignment	 ( SwingConstants.TOP	 ); 
        lblConta.setVerticalAlignment	 ( SwingConstants.TOP	 );  
        lblValor.setVerticalAlignment	 ( SwingConstants.TOP	 ); 
        
        // DEFINE O TEXTO A SER ESCRITO A DIREITA
        txtAgencia.setHorizontalAlignment( SwingConstants.RIGHT  );   
        txtConta.setHorizontalAlignment	 ( SwingConstants.RIGHT  );
        txtValor.setHorizontalAlignment	 ( SwingConstants.RIGHT  );
        
        // DEFINE O TAMANHO DE CADA CAMPO
        lblNome.setPreferredSize	 ( new Dimension( 300, 75 ) );
        lblAgencia.setPreferredSize	 ( new Dimension( 300, 75 ) );
        lblConta.setPreferredSize	 ( new Dimension( 300, 75 ) );
        lblValor.setPreferredSize	 ( new Dimension( 300, 75 ) );
        
        txtAgencia.setPreferredSize	 ( new Dimension( 300, 45 ) );
        txtConta.setPreferredSize	 ( new Dimension( 300, 45 ) );
        txtValor.setPreferredSize	 ( new Dimension( 300, 45 ) );
        
        btnConfirmar.setPreferredSize( new Dimension( 145, 75 ) );
        btnCancelar.setPreferredSize ( new Dimension( 150, 75 ) );
        
        // ATRIBUTOS DO TEXTO DE CADA CAIXA
        txtAgencia.setFont( sansSerif );
        txtConta.setFont( sansSerif );
        txtValor.setFont( sansSerif );
        
        // GRIDBAGCONSTRAINTS
        GridBagConstraints gbc1 = new GridBagConstraints();
        GridBagConstraints gbc2 = new GridBagConstraints();
        GridBagConstraints gbc3 = new GridBagConstraints();
        GridBagConstraints gbc4 = new GridBagConstraints();
        GridBagConstraints gbc5 = new GridBagConstraints();
        GridBagConstraints gbc6 = new GridBagConstraints();
        
        // ORGANIZACAO DE CADA BOTAO
        gbc1.gridx	= 0;
        gbc1.gridy	= 0; 
        gbc1.gridwidth = 2;
        
        gbc2.gridx  = 0;
        gbc2.gridy  = 1;  
        gbc2.gridwidth = 2;
        
        gbc3.gridx  = 0;
        gbc3.gridy  = 2;
        gbc3.gridwidth = 2;
        
        gbc4.gridx  = 0;
        gbc4.gridy  = 3;
        gbc4.gridwidth = 2;
        
        gbc5.gridx  = 0;
        gbc5.gridy  = 4;
        gbc5.insets = new Insets( 0, 0, 0, 5 );
        
        gbc6.gridx  = 1;
        gbc6.gridy  = 4;
       
        // ADICIONA CADA CAMPO
        add( lblNome,	   gbc1 );
        add( lblAgencia,   gbc2 );
        add( txtAgencia,   gbc2 ); 
        add( lblConta,	   gbc3 );
        add( txtConta,	   gbc3 );
        add( lblValor,	   gbc4 );
        add( txtValor,	   gbc4 ); 
        add( btnConfirmar, gbc5 ); 
        add( btnCancelar,  gbc6 );
        
        // ADICIONA FUNCAO A CADA BOTAO
        btnConfirmar.addActionListener( this );
        btnCancelar.addActionListener ( this );
	}

	@Override
	public void actionPerformed( ActionEvent evento )
	{
		if( evento.getSource() == btnConfirmar )
			JOptionPane.showMessageDialog( null, linguagem.getString( "guiTransferencia.erroManutencao"  ), linguagem.getString( "guiTransferencia.erroTitulo"  ), JOptionPane.WARNING_MESSAGE );
		else if( evento.getSource() == btnCancelar )
			dispose();
	}
}