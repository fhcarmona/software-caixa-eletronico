// PACOTE
package interfaces;

// BIBLIOTECAS
import java.text.ParseException;
import java.util.ResourceBundle;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.text.MaskFormatter;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import logica.Conta;


// CLASSE PRINCIPAL
public class GUISacarEspecifico extends JFrame implements ActionListener
{		
	// ATRIBUTOS
	private ResourceBundle linguagem;
	
	private GridBagLayout janela;
	
	private JTextField txtValor;
	
	private JButton	btnConfirmar;
	private JButton btnCancelar;
	
	private JLabel lblNome;
	private JLabel lblValor;
	
	private Conta conta = Conta.getInstancia();
		
	public GUISacarEspecifico()
	{			
		// LINGUAGEM
		this.linguagem = conta.getLinguagem();
		
		// FONTE
		Font sansSerif = new Font( "SansSerif", Font.PLAIN, 16 );
		
		// JANELA
		janela = new GridBagLayout();
        setLayout( janela );
        setTitle( linguagem.getString( "guiSacarEspecifico.titulo" ) );
        
        // NOME DOS CAMPOS
        lblNome 	 = new JLabel ( conta.getUsuario().getNome()							   );
        lblValor	 = new JLabel ( linguagem.getString( "guiSacarEspecifico.textoValor"	 ) );
        btnConfirmar = new JButton( linguagem.getString( "guiSacarEspecifico.botaoConfirmar" ) );
        btnCancelar	 = new JButton( linguagem.getString( "guiSacarEspecifico.botaoCancelar"	 ) );         
        
        // DIMENSAO DE CADA CAMPO        
        try							 { txtValor   = new JFormattedTextField( new MaskFormatter( "###" ) ); }
        catch( ParseException error ){ error.printStackTrace();											        }
        
        txtValor.setPreferredSize	 ( new Dimension( 300, 40 ) );
        lblValor.setPreferredSize	 ( new Dimension( 300, 75 ) );        
        btnConfirmar.setPreferredSize( new Dimension( 145, 75 ) );
        btnCancelar.setPreferredSize ( new Dimension( 150, 75 ) );    
        
        // LOCALIDADE DOS BOTOES
        lblNome.setHorizontalAlignment ( SwingConstants.CENTER );
        lblValor.setVerticalAlignment  ( SwingConstants.TOP	   ); 
        txtValor.setHorizontalAlignment( SwingConstants.RIGHT  );    
        
        // ATRIBUTOS DO TEXTO DE CADA CAIXA
        txtValor.setFont( sansSerif );

        // GRIDBAGCONSTRAINTS
        GridBagConstraints gbc1 = new GridBagConstraints();
        GridBagConstraints gbc2 = new GridBagConstraints();
        GridBagConstraints gbc3 = new GridBagConstraints();
        GridBagConstraints gbc4 = new GridBagConstraints();
        
        // ORGANIZACAO DE CADA BOTAO
        gbc1.gridx	   = 0;
        gbc1.gridy	   = 0;    
        gbc1.gridwidth = 2;        
        
        gbc2.gridx     = 0;
        gbc2.gridy     = 1;  
        gbc2.gridwidth = 2; 
        
        gbc3.gridx	   = 0;
        gbc3.gridy	   = 2;
        gbc3.insets	   = new Insets( 0, 0, 0, 5 );
        
        gbc4.gridx	   = 1;
        gbc4.gridy	   = 2;
       
        // ADICIONA CADA BOTAO
        add( lblNome,	   gbc1 );
        add( lblValor,	   gbc2 );
        add( txtValor,	   gbc2 );
        add( btnConfirmar, gbc3 );
        add( btnCancelar,  gbc4 );
        
        // ADICIONA FUNCAO A CADA BOTAO
        btnConfirmar.addActionListener( this );
        btnCancelar.addActionListener ( this );
	}

	@Override
	public void actionPerformed( ActionEvent evento )
	{
		if( evento.getSource() == btnConfirmar )
		{
			if( Integer.parseInt( txtValor.getText() ) > 800 )
			{
				JOptionPane.showMessageDialog( null, linguagem.getString( "guiSacarEspecifico.erroValorDia" ), linguagem.getString( "guiSacarEspecifico.erroTitulo" ), JOptionPane.ERROR_MESSAGE );
				
				return;
			}
			else if( Integer.parseInt( txtValor.getText() ) % 5 != 0 )
			{
				JOptionPane.showMessageDialog( null, linguagem.getString( "guiSacarEspecifico.erroNotas" ), linguagem.getString( "guiSacarEspecifico.erroTitulo" ), JOptionPane.ERROR_MESSAGE );
				
				return;
			}
			
			GUIToken objToken = new GUIToken( Integer.parseInt( txtValor.getText() ) );
			
			objToken.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );			
			objToken.setSize				 ( 800, 600 );
			objToken.setResizable			 ( false );			
			objToken.setVisible				 ( true );
			objToken.setLocationRelativeTo	 ( null );
			
			dispose();
		}
		else if( evento.getSource() == btnCancelar )
		{
			dispose();
		}
	}
}