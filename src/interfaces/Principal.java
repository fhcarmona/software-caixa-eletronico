// PACOTE
package interfaces;

//IMPORTA BIBLIOTECAS
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import logica.ArquivoTXT;
import logica.Conta;
import logica.Criptografia;

// CLASS PRINCIPAL
public class Principal
{
	public static void main( String args[] )
	{			
		// INSTANCIA O RESPONSAVEL PELA LINGUAGEM
		ResourceBundle linguagem = null;
		
		// INSTANCIA O PAINEL
		JPanel panel = new JPanel();
		
		// INSTANCIA O GRUPO DO RADIOBUTTON
		ButtonGroup grupoLinguagem = new ButtonGroup();
		
		// INSTANCIA O JRADIOBUTTON
		JRadioButton opcaoPT = new JRadioButton( "PT-BR" );
		JRadioButton opcaoUS = new JRadioButton( "EN-US" );
		JRadioButton opcaoES = new JRadioButton( "ES-AR" );
		
		// ADICIONA OS RADIO BUTTONS NO GRUPO
		grupoLinguagem.add( opcaoPT );
		grupoLinguagem.add( opcaoUS );
		grupoLinguagem.add( opcaoES );
		
		// ADICIONA OS RADIOBUTTON EM UM ARRAY DE OBJETOS
		Object[] radioButtonArray = { opcaoPT, opcaoUS, opcaoES };
		
		// ASSINALA A OPCAO PT COMO PADRAO
		opcaoPT.setSelected( true );
		
		// ESCOLHE O IDIOMA DEFINIDO NO ARRAY DE OBJETOS RADIOBUTTON
		JOptionPane.showConfirmDialog( null, radioButtonArray, "", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE );
		
		// MODIFICA A LINGUAGEM DE ACORDO COM A NECESSIDADE
		if( opcaoPT.isSelected() )
			linguagem = ResourceBundle.getBundle( "linguagens.pt-br", new Locale( "pt", "BR" ) );
		else if( opcaoUS.isSelected() )
			linguagem = ResourceBundle.getBundle( "linguagens.en-us", Locale.US );
		else if( opcaoES.isSelected() )
			linguagem = ResourceBundle.getBundle( "linguagens.es-ar", new Locale( "es", "AR" ) );
		
		// SETA A LINGUAGEM PARA O PROJETO
		Conta conta = Conta.getInstancia();
		
		conta.setLinguagem( linguagem );
		
		// INTANCIA O ACESSO
		ArquivoTXT objArquivo = new ArquivoTXT();
		
		// CRIA O ARQUIVO TXT COM OS DADOS CONTIDOS NO BANCO
		try
		{
			objArquivo.criarArquivoTXT();
		}
		catch( Exception erro )
		{
			erro.printStackTrace();
			System.exit( 1 ); 
		}
		
		// INSTANCIA A INTERFACE DE ACESSO
		GUIAcesso objGUIAcesso = new GUIAcesso();
		
		objGUIAcesso.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );			
		objGUIAcesso.setSize( 800, 600 );
		objGUIAcesso.setResizable( false );			
		objGUIAcesso.setVisible( true );
		objGUIAcesso.setLocationRelativeTo( null );	
	}
}
