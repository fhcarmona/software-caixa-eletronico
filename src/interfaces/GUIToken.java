// PACOTE
package interfaces;

// BIBLIOTECAS
import java.util.ResourceBundle;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import logica.Conta;
import logica.Token;

// CLASSE PRINCIPAL
public class GUIToken extends JFrame implements ActionListener
{		
	// ATRIBUTOS
	private GridBagLayout janela;
	
	private JButton btnX0Y0;
	private JButton btnX1Y0;
	private JButton btnX0Y1;
	private JButton btnX1Y1;
	private JButton btnX0Y2;
	private JButton btnX1Y2;
	
	private JTextField txtToken;
	
	private ResourceBundle linguagem;

	private Conta conta	   = Conta.getInstancia();
	private Token objToken = new Token();
	
	private String[] nomeBotoes = new String[6];
	
	private int valor;
	
	public GUIToken( int valor )
	{			
		// FONTE
		Font sansSerif = new Font( "SansSerif", Font.PLAIN, 16 );
		
		// LINGUAGEM
		this.linguagem = conta.getLinguagem();
		
		this.valor = valor;
		
		// JANELA
		janela = new GridBagLayout();
        setLayout( janela );
        setTitle( linguagem.getString( "guiToken.titulo" ) );
        
        // ESQUEMA BOTOES
        btnX0Y0	= new JButton( nomeBotoes[0] );
        btnX1Y0	= new JButton( nomeBotoes[1] );
        btnX0Y1	= new JButton( nomeBotoes[2] );
        btnX1Y1	= new JButton( nomeBotoes[3] );
        btnX0Y2	= new JButton( nomeBotoes[4] );
        btnX1Y2	= new JButton( nomeBotoes[5] );
        
        escreveNumeros();
        
        txtToken = new JTextField( 5 );
        
        // DIMENSAO DE CADA BOTAO
        btnX0Y0.setPreferredSize ( new Dimension( 300, 75 ) );
        btnX1Y0.setPreferredSize ( new Dimension( 300, 75 ) );
        btnX0Y1.setPreferredSize ( new Dimension( 300, 75 ) );
        btnX1Y1.setPreferredSize ( new Dimension( 300, 75 ) );
        btnX0Y2.setPreferredSize ( new Dimension( 300, 75 ) );
        btnX1Y2.setPreferredSize ( new Dimension( 300, 75 ) );
        
        txtToken.setPreferredSize( new Dimension( 300, 35 ) );
        
        // MUDA A FONTE DO TXT
        txtToken.setFont( sansSerif );
        
        txtToken.setHorizontalAlignment	 ( SwingConstants.CENTER ); 
        
        // GRIDBAGCONSTRAINTS
        GridBagConstraints gbc1 = new GridBagConstraints();
        GridBagConstraints gbc2 = new GridBagConstraints();
        GridBagConstraints gbc3 = new GridBagConstraints();
        GridBagConstraints gbc4 = new GridBagConstraints();
        GridBagConstraints gbc5 = new GridBagConstraints();
        GridBagConstraints gbc6 = new GridBagConstraints();
        GridBagConstraints gbc7 = new GridBagConstraints();
        GridBagConstraints gbc8 = new GridBagConstraints();
        GridBagConstraints gbc9 = new GridBagConstraints();
        
        // ORGANIZACAO DE CADA BOTAO 
        gbc1.gridx  = 0;
        gbc1.gridy  = 1;  
        gbc1.insets = new Insets( 0, 0, 5, 5 );
        
        gbc2.gridx  = 1;
        gbc2.gridy  = 1;
        gbc2.insets = new Insets( 0, 0, 5, 0 );
        
        gbc3.gridx  = 0;
        gbc3.gridy  = 2;
        gbc3.insets = new Insets( 0, 0, 5, 5 );
        
        gbc4.gridx  = 1;
        gbc4.gridy  = 2;
        gbc4.insets = new Insets( 0, 0, 5, 0 );
        
        gbc5.gridx  = 0;
        gbc5.gridy  = 3; 
        gbc5.insets = new Insets( 0, 0, 0, 5 );
        
        gbc6.gridx  = 1;
        gbc6.gridy  = 3;
        
        gbc7.gridx	   = 0;
        gbc7.gridy	   = 4;
        gbc7.gridwidth = 2;
        gbc7.insets	   = new Insets( 100, 0, 0, 0 );
        
        // ADICIONA CADA BOTAO
        add( btnX0Y0,  gbc1 );
        add( btnX1Y0,  gbc2 );
        add( btnX0Y1,  gbc3 );
        add( btnX1Y1,  gbc4 );
        add( btnX0Y2,  gbc5 );
        add( btnX1Y2,  gbc6 );
        add( txtToken, gbc7 ); 
        
        // BLOQUEIA O CAMPO DE TEXTO
        txtToken.setEditable( false );
        
        // ADICIONA FUNCAO A CADA BOTAO
        btnX0Y0.addActionListener( this );
        btnX1Y0.addActionListener( this );
        btnX0Y1.addActionListener( this );
        btnX1Y1.addActionListener( this );
        btnX0Y2.addActionListener( this );
        btnX1Y2.addActionListener( this );
	}
	
	@Override
	public void actionPerformed( ActionEvent evento )
	{		
		// ESCREVE O VALOR DO BOTAO NO TXTTOKEN
		if( evento.getSource() == btnX0Y0 )
			txtToken.setText( txtToken.getText() + btnX0Y0.getText() );
		else if( evento.getSource() == btnX1Y0 )
			txtToken.setText( txtToken.getText() + btnX1Y0.getText() );
		else if( evento.getSource() == btnX0Y1 )
			txtToken.setText( txtToken.getText() + btnX0Y1.getText() );
		else if( evento.getSource() == btnX1Y1 )
			txtToken.setText( txtToken.getText() + btnX1Y1.getText() );
		else if( evento.getSource() == btnX0Y2 )
			txtToken.setText( txtToken.getText() + btnX0Y2.getText() );
		else if( evento.getSource() == btnX1Y2 )
			txtToken.setText( txtToken.getText() + btnX1Y2.getText() );
			
		// VERIFICA SE O TOKEN E COMPATIVEL, APOS A TERCEIRA TENTATIVA
		validaRetirada();		
		
		// MUDA O NOME DOS BOTOES
		escreveNumeros();
	}
	
	// MODIFICA OS NOME DOS BOTOES
	public void escreveNumeros()
	{
		this.nomeBotoes = objToken.geraValoresToken( conta.getUsuario().getToken() );
		
		btnX0Y0.setText( nomeBotoes[0] );
		btnX1Y0.setText( nomeBotoes[1] );
		btnX0Y1.setText( nomeBotoes[2] );
		btnX1Y1.setText( nomeBotoes[3] );
		btnX0Y2.setText( nomeBotoes[4] );
		btnX1Y2.setText( nomeBotoes[5] );
	}
	
	public void validaRetirada()
	{
		if( objToken.verificaPreenchimento() )
		{
			if( objToken.validarToken( txtToken.getText() ) )
			{
				if( !objToken.retirarDinheiro( valor ) )
				{
					JOptionPane.showMessageDialog( null, linguagem.getString( "guiToken.erroSaldo"), linguagem.getString( "guiToken.tituloAtencao"), JOptionPane.WARNING_MESSAGE );
					dispose();
					return;
				}
				
				JOptionPane.showMessageDialog( null, linguagem.getString( "guiToken.sucessoSacar"), linguagem.getString( "guiToken.tituloAtencao"), JOptionPane.INFORMATION_MESSAGE );
				
				conta.resetaTentativas();
			}
			else
			{
				conta.erroTentativa();
				
				if( conta.getTentativas() == 0 )
				{
					JOptionPane.showMessageDialog( null, linguagem.getString( "guiToken.erroTravado"), linguagem.getString( "guiToken.tituloErro"), JOptionPane.WARNING_MESSAGE );
					System.exit(0);
				}
				
				JOptionPane.showMessageDialog( null, linguagem.getString( "guiToken.erroToken") + " 0" + conta.getTentativas(), linguagem.getString( "guiToken.tituloErro"), JOptionPane.WARNING_MESSAGE );
			}
			
			dispose();
		}
	}
}