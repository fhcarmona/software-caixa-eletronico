// PACOTE
package interfaces;

// BIBLIOTECAS
import java.util.ResourceBundle;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import logica.Conta;

// CLASSE PRINCIPAL
public class GUIPrincipal extends JFrame implements ActionListener
{		
	// ATRIBUTOS
	private GridBagLayout janela;
	
	private JButton btnExtrato;
	private JButton btnSaldo;
	private JButton btnTransferir;
	private JButton btnDebito;
	private JButton btnSacar;
	private JButton btnCancelar;
	
	private JLabel  lblNome;
	
	private ResourceBundle linguagem;
	
	private Conta conta = Conta.getInstancia();
	
	public GUIPrincipal()
	{
		this.linguagem = conta.getLinguagem();
		
		janela = new GridBagLayout();
		setTitle( linguagem.getString( "guiPrincipal.titulo" ) );
        setLayout( janela );
        
        // ATRIBUTOS DO LABEL
        lblNome = new JLabel( conta.getUsuario().getNome() );
        lblNome.setPreferredSize( new Dimension( 300, 75 ) );
        lblNome.setHorizontalAlignment( SwingConstants.CENTER );  
        
        // NOME DOS BOTOES
        btnExtrato	  = new JButton( linguagem.getString( "guiPrincipal.botaoExtrato"	 ) );
        btnSaldo	  = new JButton( linguagem.getString( "guiPrincipal.botaoSaldo"		 ) );
        btnTransferir = new JButton( linguagem.getString( "guiPrincipal.botaoTransferir" ) );
        btnDebito	  = new JButton( linguagem.getString( "guiPrincipal.botaoDebito"	 ) );
        btnSacar	  = new JButton( linguagem.getString( "guiPrincipal.botaoSacar"		 ) );
        btnCancelar	  = new JButton( linguagem.getString( "guiPrincipal.botaoCancelar"	 ) );
        
        // DIMENSAO DE CADA BOTAO
        btnExtrato.setPreferredSize	  ( new Dimension( 300, 75 ) );
        btnSaldo.setPreferredSize	  ( new Dimension( 300, 75 ) );
        btnTransferir.setPreferredSize( new Dimension( 300, 75 ) );
        btnDebito.setPreferredSize	  ( new Dimension( 300, 75 ) );
        btnSacar.setPreferredSize	  ( new Dimension( 300, 75 ) );
        btnCancelar.setPreferredSize  ( new Dimension( 300, 75 ) );
        
        // GRIDBAGCONSTRAINTS
        GridBagConstraints gbc1 = new GridBagConstraints();
        GridBagConstraints gbc2 = new GridBagConstraints();
        GridBagConstraints gbc3 = new GridBagConstraints();
        GridBagConstraints gbc4 = new GridBagConstraints();
        GridBagConstraints gbc5 = new GridBagConstraints();
        GridBagConstraints gbc6 = new GridBagConstraints();
        GridBagConstraints gbc7 = new GridBagConstraints();
        
        // ORGANIZACAO DE CADA BOTAO
        gbc1.gridx     = 0;
        gbc1.gridy 	   = 0; 
        gbc1.gridwidth = 2;
        
        gbc2.gridx = 0;
        gbc2.gridy = 1; 
        gbc2.insets = new Insets( 0, 0, 5, 5 );
        
        gbc3.gridx = 1;
        gbc3.gridy = 1;
        gbc3.insets = new Insets( 0, 0, 5, 0 );
        
        gbc4.gridx = 0;
        gbc4.gridy = 2;
        gbc4.insets = new Insets( 0, 0, 5, 5 );
        
        gbc5.gridx = 1;
        gbc5.gridy = 2;
        gbc5.insets = new Insets( 0, 0, 5, 0 );
        
        gbc6.gridx = 0;
        gbc6.gridy = 3;  
        gbc6.insets = new Insets( 0, 0, 0, 5 );
        
        gbc7.gridx = 1;
        gbc7.gridy = 3;
       
        // ADICIONA CADA BOTAO
        add( lblNome,		gbc1 );
        add( btnExtrato,	gbc2 );
        add( btnSaldo,		gbc3 );
        add( btnTransferir, gbc4 );
        add( btnDebito,		gbc5 );
        add( btnSacar,		gbc6 );
        add( btnCancelar,	gbc7 );
        
        // ADICIONA FUNCAO A CADA BOTAO
        btnExtrato.addActionListener   ( this );
        btnSaldo.addActionListener	   ( this );
        btnTransferir.addActionListener( this );
        btnDebito.addActionListener	   ( this );
        btnSacar.addActionListener	   ( this );
        btnCancelar.addActionListener  ( this );
	}

	@Override
	public void actionPerformed( ActionEvent evento )
	{
		if( evento.getSource() == btnExtrato )
		{
			chamarGUIExtrato();
		}
		else if( evento.getSource() == btnSaldo )
		{
			chamarGUISaldo();
		}
		else if( evento.getSource() == btnTransferir )
		{
			chamarGUITransferir();
		}
		else if( evento.getSource() == btnDebito )
		{
			chamarGUIDebito();
		}
		else if( evento.getSource() == btnSacar )
		{
			chamarGUISacar();
		}
		else if( evento.getSource() == btnCancelar )
		{
			dispose();
		}
	}
	
	public void chamarGUIExtrato()
	{
		GUIPeriodoExtrato objExtrato = new GUIPeriodoExtrato();
		
		objExtrato.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );			
		objExtrato.setSize				   ( 800, 600 );
		objExtrato.setResizable			   ( false );			
		objExtrato.setVisible			   ( true );
		objExtrato.setLocationRelativeTo   ( null );
	}
	
	public void chamarGUISaldo()
	{
		GUIRelatorios objRelatorioSaldo = new GUIRelatorios( 2 );
		
		objRelatorioSaldo.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );			
		objRelatorioSaldo.setSize				  ( 800, 600 );
		objRelatorioSaldo.setResizable			  ( false );			
		objRelatorioSaldo.setVisible			  ( true );
		objRelatorioSaldo.setLocationRelativeTo	  ( null );
	}
	
	public void chamarGUITransferir()
	{
		GUITransferencia objTransferir = new GUITransferencia();
		
		objTransferir.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );			
		objTransferir.setSize				  ( 800, 600 );
		objTransferir.setResizable			  ( false );			
		objTransferir.setVisible			  ( true );
		objTransferir.setLocationRelativeTo	  ( null );
	}
	
	public void chamarGUIDebito()
	{
		GUICadastrarContaDebito objDebito = new GUICadastrarContaDebito();
		
		objDebito.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );			
		objDebito.setSize				  ( 800, 600 );
		objDebito.setResizable			  ( false );			
		objDebito.setVisible			  ( true );
		objDebito.setLocationRelativeTo	  ( null );
	}
	
	public void chamarGUISacar()
	{
		GUISacar objSacar = new GUISacar();
		
		objSacar.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );			
		objSacar.setSize				 ( 800, 600 );
		objSacar.setResizable			 ( false );			
		objSacar.setVisible				 ( true );
		objSacar.setLocationRelativeTo	 ( null );
	}
}