//PACOTE
package interfaces;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import logica.Relatorios;
import logica.Conta;

//BIBLIOTECAS

//CLASSE PRINCIPAL
public class GUIRelatorios extends JFrame implements ActionListener
{		
	// ATRIBUTOS
	private GridBagLayout janela;
		
	private JLabel lblNome;
	
	private JTextArea txtRelatorio;
	
	private JButton	btnImprimir;
	private JButton btnCancelar;
	
	private ResourceBundle linguagem;
	
	private String mensagemImpressao;
	private String tituloImpressao;
	
	private String dataInicio;
	private String dataFinal;
	
	private Conta conta = Conta.getInstancia();
	
	private Relatorios relatorio = new Relatorios();
	
	// CONSTRUTOR PARA O EXTRATO
	public GUIRelatorios( String dataInicio, String dataFinal, int tipo )
	{
		// ATRIBUI A LINGUAGEM ESCOLHIDA NA VARIAVEL DA CLASSE
		this.linguagem = conta.getLinguagem();
		
		this.dataInicio = dataInicio;
		this.dataFinal  = dataFinal;
		
		// DEFINE AS MENSAGENS DE ERRO PARA A IMPRESSAO
		this.mensagemImpressao = linguagem.getString( "guiRelatorios.textoMensagemImpressao" );
		this.tituloImpressao   = linguagem.getString( "guiRelatorios.textoTituloImpressao"   );
		
		// DEFINE UMA FONTE CUSTOMIZADA PARA O RELATORIO
		Font sansSerif = new Font( "SansSerif", Font.PLAIN, 16 );
		
		janela = new GridBagLayout();
		setLayout( janela );
		setTitle( linguagem.getString( "guiRelatorios.titulo" ) );
		 
		// INSTANCIA OS CAMPOS
		lblNome		 = new JLabel( conta.getUsuario().getNome() );
		 
		btnImprimir  = new JButton( linguagem.getString( "guiRelatorios.botaoImprimir" ) );
		btnCancelar  = new JButton( linguagem.getString( "guiRelatorios.botaoCancelar" ) );
		 
		txtRelatorio = new JTextArea();

		// ATRIBUI LOCALIDADE A ETIQUETA
		lblNome.setHorizontalAlignment( SwingConstants.CENTER ); 
		 
		// DEFINE O TAMANHO DE CADA CAMPO
		lblNome.setPreferredSize	 ( new Dimension( 300, 35  ) );
		  
		txtRelatorio.setPreferredSize( new Dimension( 750, 425 ) );
		        
		btnImprimir.setPreferredSize ( new Dimension( 145, 75  ) );
		btnCancelar.setPreferredSize ( new Dimension( 150, 75  ) );
		 
		// BLOQUEIA TELA DE RELATORIO
		txtRelatorio.setEditable( false );
		 
		// ATRIBUTOS DA CAIXA DE TEXTO
		txtRelatorio.setFont	  ( sansSerif );
		txtRelatorio.setBackground( new Color( 252, 255, 194, 100 ) );
		txtRelatorio.setBorder	  ( BorderFactory.createLineBorder( new Color( 123, 138, 156, 100 ) ) );
		 
		// GRIDBAGCONSTRAINTS
		GridBagConstraints gbc1 = new GridBagConstraints();
		GridBagConstraints gbc2 = new GridBagConstraints();
		GridBagConstraints gbc3 = new GridBagConstraints();
		GridBagConstraints gbc4 = new GridBagConstraints();
		 
		// ORGANIZACAO DE CADA BOTAO
		gbc1.gridx	   = 0;
		gbc1.gridy	   = 0; 
		gbc1.gridwidth = 6;
		 
		gbc2.gridx	   = 0;
		gbc2.gridy	   = 1;  
		gbc2.gridwidth = 6;
		gbc2.insets	   = new Insets( 0, 0, 5, 0 );
		 
		gbc3.gridx  = 0;
		gbc3.gridy  = 2;
		gbc3.insets = new Insets( 0, 0, 0, 5 );
		 
		gbc4.gridx  = 1;
		gbc4.gridy  = 2;
		
		// ADICIONA CADA CAMPO
		add( lblNome,	   gbc1 );
		add( txtRelatorio, gbc2 ); 
		add( btnImprimir,  gbc3 ); 
		add( btnCancelar,  gbc4 );
		 
		// ADICIONA FUNCAO A CADA BOTAO
		btnImprimir.addActionListener( this );
		btnCancelar.addActionListener( this );
		
		// ESCREVE O RELATORIO
		txtRelatorio.setText( relatorio.escreverRelatorio( 1, dataInicio, dataFinal ) );
	}
	
	// CONSTRUTOR PARA AS DEMAIS JANELAS
	public GUIRelatorios( int tipoRelatorio )
	{			
		// ATRIBUI A LINGUAGEM ESCOLHIDA NA VARIAVEL DA CLASSE
		this.linguagem = conta.getLinguagem();
		
		// DEFINE AS MENSAGENS DE ERRO PARA A IMPRESSAO
		this.mensagemImpressao = linguagem.getString( "guiRelatorios.textoMensagemImpressao" );
		this.tituloImpressao   = linguagem.getString( "guiRelatorios.textoTituloImpressao"   );
		
		// DEFINE UMA FONTE CUSTOMIZADA PARA O RELATORIO
		Font sansSerif = new Font( "SansSerif", Font.PLAIN, 16 );
		
		janela = new GridBagLayout();
		setLayout( janela );
		setTitle( linguagem.getString( "guiRelatorios.titulo" ) );
		 
		// INSTANCIA OS CAMPOS
		lblNome		 = new JLabel( conta.getUsuario().getNome() );
		 
		btnImprimir  = new JButton( linguagem.getString( "guiRelatorios.botaoImprimir" ) );
		btnCancelar  = new JButton( linguagem.getString( "guiRelatorios.botaoCancelar" ) );
		 
		txtRelatorio = new JTextArea();
		 
		// ATRIBUI LOCALIDADE A ETIQUETA
		lblNome.setHorizontalAlignment( SwingConstants.CENTER ); 
		 
		// DEFINE O TAMANHO DE CADA CAMPO
		lblNome.setPreferredSize	 ( new Dimension( 300, 35  ) );
		  
		txtRelatorio.setPreferredSize( new Dimension( 750, 425 ) );
		        
		btnImprimir.setPreferredSize ( new Dimension( 145, 75  ) );
		btnCancelar.setPreferredSize ( new Dimension( 150, 75  ) );
		 
		// BLOQUEIA TELA DE RELATORIO
		txtRelatorio.setEditable( false );
		 
		// ATRIBUTOS DA CAIXA DE TEXTO
		txtRelatorio.setFont	  ( sansSerif );
		txtRelatorio.setBackground( new Color( 252, 255, 194, 100 ) );
		txtRelatorio.setBorder	  ( BorderFactory.createLineBorder( new Color( 123, 138, 156, 100 ) ) );
		 
		// GRIDBAGCONSTRAINTS
		GridBagConstraints gbc1 = new GridBagConstraints();
		GridBagConstraints gbc2 = new GridBagConstraints();
		GridBagConstraints gbc3 = new GridBagConstraints();
		GridBagConstraints gbc4 = new GridBagConstraints();
		 
		// ORGANIZACAO DE CADA BOTAO
		gbc1.gridx	   = 0;
		gbc1.gridy	   = 0; 
		gbc1.gridwidth = 6;
		 
		gbc2.gridx	   = 0;
		gbc2.gridy	   = 1;  
		gbc2.gridwidth = 6;
		gbc2.insets	   = new Insets( 0, 0, 5, 0 );
		 
		gbc3.gridx  = 0;
		gbc3.gridy  = 2;
		gbc3.insets = new Insets( 0, 0, 0, 5 );
		 
		gbc4.gridx  = 1;
		gbc4.gridy  = 2;
		
		// ADICIONA CADA CAMPO
		add( lblNome,	   gbc1 );
		add( txtRelatorio, gbc2 ); 
		add( btnImprimir,  gbc3 ); 
		add( btnCancelar,  gbc4 );
		 
		// ADICIONA FUNCAO A CADA BOTAO
		btnImprimir.addActionListener( this );
		btnCancelar.addActionListener( this );
		
		// ESCREVE O RELATORIO
		txtRelatorio.setText( relatorio.escreverRelatorio( tipoRelatorio, "", "" ) );	
	}

	@Override
	public void actionPerformed( ActionEvent evento )
	{
		if( evento.getSource() == btnImprimir )
			JOptionPane.showMessageDialog( null, mensagemImpressao, tituloImpressao, JOptionPane.ERROR_MESSAGE );
		else if( evento.getSource() == btnCancelar )
			dispose();
	}
}