// PACOTE
package interfaces;

// BIBLIOTECAS
import java.text.ParseException;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel; 
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.text.MaskFormatter;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import logica.Conta;


// CLASSE PRINCIPAL
public class GUIPeriodoExtrato extends JFrame implements ActionListener
{		
	// ATRIBUTOS
	private GridBagLayout janela;
	
	private JButton btnConfirmar;
	private JButton btnCancelar;
	
	private JTextField txtDataInicial;
	private JTextField txtDataFinal;
	
	private JLabel lblNome;
	private JLabel lblDataInicial;
	private JLabel lblDataFinal;
	
	private ResourceBundle linguagem;
	
	private Conta conta = Conta.getInstancia();
	
	public GUIPeriodoExtrato()
	{			
		this.linguagem = conta.getLinguagem();
		
		Font sansSerif = new Font( "SansSerif", Font.PLAIN, 16 );
		
		janela = new GridBagLayout();
        setLayout( janela );
        setTitle( linguagem.getString( "guiPeriodoExtrato.titulo" ) );
        
        // INSTANCIA OS CAMPOS
        lblNome		   = new JLabel( conta.getUsuario().getNome() );
        lblDataInicial = new JLabel( linguagem.getString( "guiPeriodoExtrato.textoDataInicial" ) );
        lblDataFinal   = new JLabel( linguagem.getString( "guiPeriodoExtrato.textoDataFinal"  ) );
        
        btnConfirmar = new JButton( linguagem.getString( "guiPeriodoExtrato.botaoConfirmar" ) );
        btnCancelar  = new JButton( linguagem.getString( "guiPeriodoExtrato.botaoCancelar"  ) );
        
        try
        {
        	txtDataInicial = new JFormattedTextField( new MaskFormatter( "##/##/####"  ) );
        	txtDataFinal   = new JFormattedTextField( new MaskFormatter( "##/##/####" ) );
		}
        catch( ParseException error ){ error.printStackTrace();	}
        
        // ATRIBUI LOCALIDADE A CADA ETIQUETA
        lblNome.setHorizontalAlignment	   ( SwingConstants.CENTER ); 
        lblDataInicial.setVerticalAlignment( SwingConstants.TOP	 ); 
        lblDataFinal.setVerticalAlignment  ( SwingConstants.TOP	 );  
        
        // DEFINE O TAMANHO DE CADA CAMPO
        lblNome.setPreferredSize	   ( new Dimension( 300, 75 ) );
        lblDataInicial.setPreferredSize( new Dimension( 300, 75 ) );
        lblDataFinal.setPreferredSize  ( new Dimension( 300, 75 ) );
        
        txtDataInicial.setPreferredSize( new Dimension( 300, 45 ) );
        txtDataFinal.setPreferredSize  ( new Dimension( 300, 45 ) );
        
        btnConfirmar.setPreferredSize  ( new Dimension( 145, 75 ) );
        btnCancelar.setPreferredSize   ( new Dimension( 150, 75 ) );
        
        // ATRIBUTOS DO TEXTO DE CADA CAIXA
        txtDataInicial.setFont( sansSerif );
        txtDataFinal.setFont  ( sansSerif );
        
        // GRIDBAGCONSTRAINTS
        GridBagConstraints gbc1 = new GridBagConstraints();
        GridBagConstraints gbc2 = new GridBagConstraints();
        GridBagConstraints gbc3 = new GridBagConstraints();
        GridBagConstraints gbc4 = new GridBagConstraints();
        GridBagConstraints gbc5 = new GridBagConstraints();
        
        // ORGANIZACAO DE CADA BOTAO
        gbc1.gridx	= 0;
        gbc1.gridy	= 0; 
        gbc1.gridwidth = 2;
        
        gbc2.gridx  = 0;
        gbc2.gridy  = 1;  
        gbc2.gridwidth = 2;
        
        gbc3.gridx  = 0;
        gbc3.gridy  = 2;
        gbc3.gridwidth = 2;
        
        gbc4.gridx  = 0;
        gbc4.gridy  = 4;
        gbc4.insets = new Insets( 0, 0, 0, 5 );
        
        gbc5.gridx  = 1;
        gbc5.gridy  = 4;
       
        // ADICIONA CADA CAMPO
        add( lblNome,	     gbc1 );
        add( lblDataInicial, gbc2 );
        add( txtDataInicial, gbc2 ); 
        add( lblDataFinal,	 gbc3 );
        add( txtDataFinal,	 gbc3 );
        add( btnConfirmar,	 gbc4 ); 
        add( btnCancelar,	 gbc5 );
        
        // ADICIONA FUNCAO A CADA BOTAO
        btnConfirmar.addActionListener( this );
        btnCancelar.addActionListener ( this );
	}

	@Override
	public void actionPerformed( ActionEvent evento )
	{
		if( evento.getSource() == btnConfirmar )	 { chamarGUIRelatorios(); }
		else if( evento.getSource() == btnCancelar ) { dispose();			  }
	}
	
	public void chamarGUIRelatorios()
	{
		// CONVERTE DO FORMATO BRASILEIRO PARA O FORMATO DO BANCO
		String dataInicialBD = txtDataInicial.getText().substring( 6 ) + "-" + txtDataInicial.getText().substring( 3, 5 ) + "-" + txtDataInicial.getText().substring( 0, 2 );
		String dataFinalBD   =   txtDataFinal.getText().substring( 6 ) + "-" +   txtDataFinal.getText().substring( 3, 5 ) + "-" +   txtDataFinal.getText().substring( 0, 2 );
		
		if( dataInicialBD.equals( "    -  -  " ) || dataFinalBD.equals( "    -  -  " ) )
		{
			JOptionPane.showMessageDialog( null, linguagem.getString( "guiPeriodoExtrato.erroTexto" ), linguagem.getString( "guiPeriodoExtrato.erroTitulo" ), JOptionPane.WARNING_MESSAGE );
		}
		else
		{
			GUIRelatorios objRelatorio = new GUIRelatorios( dataInicialBD, dataFinalBD, 1 );
			
			objRelatorio.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );			
			objRelatorio.setSize				 ( 800, 600				   );
			objRelatorio.setResizable			 ( false 				   );			
			objRelatorio.setVisible				 ( true					   );
			objRelatorio.setLocationRelativeTo	 ( null					   );
			
			dispose(); 
		}
	}
}