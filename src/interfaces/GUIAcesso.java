// AUTORES: BRUNO, FABIO E LEONARDO
// 
// DESCRICAO:
//		CLASSE RESPONSAVEL PELO VISUAL E CHAMADA DO METODO DE LOGIN

// PACOTE
package interfaces;

// BIBLIOTECAS
import java.text.ParseException;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.ResourceBundle;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.text.MaskFormatter;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import logica.Acesso;
import logica.Conta;
import logica.Usuario;

// CLASSE PRINCIPAL
public class GUIAcesso extends JFrame implements ActionListener
{		
	// ATRIBUTOS
	private GridBagLayout janela;
		
	private JLabel lblAgencia;
	private JLabel lblConta;
	private JLabel lblSenha;
	
	private JTextField	   txtAgencia;
	private JTextField	   txtConta;
	private JPasswordField txtSenha;
	
	private JButton	btnAcessar;
	private JButton btnCancelar;
	
	private ResourceBundle linguagem;
	
	private Conta conta = Conta.getInstancia();
	
	public GUIAcesso()
	{			
		Font sansSerif = new Font( "SansSerif", Font.PLAIN, 16 );
		
		this.linguagem = conta.getLinguagem();
		
		janela = new GridBagLayout();
        setLayout( janela );
        setTitle( linguagem.getString("guiAcesso.titulo") );
             
        // INSTANCIA OS CAMPOS
        lblAgencia = new JLabel( linguagem.getString( "guiAcesso.textoAgencia"  ) );
        lblConta   = new JLabel( linguagem.getString( "guiAcesso.textoConta"	) );
        lblSenha   = new JLabel( linguagem.getString( "guiAcesso.textoSenha"	) );
        
        btnAcessar 	= new JButton( linguagem.getString( "guiAcesso.botaoAcessar"  ) );
        btnCancelar = new JButton( linguagem.getString( "guiAcesso.botaoCancelar" ) );
        
        try
        {
			txtAgencia = new JFormattedTextField( new MaskFormatter( "#####"  ) );
			txtConta   = new JFormattedTextField( new MaskFormatter( "######" ) );
			txtSenha   = new JPasswordField		( 23 );
		}
        catch( ParseException error ){ error.printStackTrace();	}
        
        // ATRIBUI LOCALIDADE A CADA ETIQUETA
        lblAgencia.setVerticalAlignment	 ( SwingConstants.TOP	 ); 
        lblConta.setVerticalAlignment	 ( SwingConstants.TOP	 );  
        lblSenha.setVerticalAlignment	 ( SwingConstants.TOP	 ); 
        
        // DEFINE O TEXTO A SER ESCRITO A DIREITA
        txtAgencia.setHorizontalAlignment( SwingConstants.RIGHT  );   
        txtConta.setHorizontalAlignment	 ( SwingConstants.RIGHT  );
        txtSenha.setHorizontalAlignment	 ( SwingConstants.RIGHT  );
        
        // DEFINE O TAMANHO DE CADA CAMPO
        lblAgencia.setPreferredSize	 ( new Dimension( 300, 75 ) );
        lblConta.setPreferredSize	 ( new Dimension( 300, 75 ) );
        lblSenha.setPreferredSize	 ( new Dimension( 300, 75 ) );
        
        txtAgencia.setPreferredSize	 ( new Dimension( 300, 45 ) );
        txtConta.setPreferredSize	 ( new Dimension( 300, 45 ) );
        txtSenha.setPreferredSize	 ( new Dimension( 300, 45 ) );
        
        btnAcessar.setPreferredSize( new Dimension( 145, 75 ) );
        btnCancelar.setPreferredSize ( new Dimension( 150, 75 ) );
        
        // ATRIBUTOS DO TEXTO DE CADA CAIXA
        txtAgencia.setFont( sansSerif );
        txtConta.setFont( sansSerif );
        txtSenha.setFont( sansSerif );
        
        // GRIDBAGCONSTRAINTS
        GridBagConstraints gbc1 = new GridBagConstraints();
        GridBagConstraints gbc2 = new GridBagConstraints();
        GridBagConstraints gbc3 = new GridBagConstraints();
        GridBagConstraints gbc4 = new GridBagConstraints();
        GridBagConstraints gbc5 = new GridBagConstraints();
        
        // ORGANIZACAO DE CADA BOTAO
        gbc1.gridx  = 0;
        gbc1.gridy  = 1;  
        gbc1.gridwidth = 2;
        
        gbc2.gridx  = 0;
        gbc2.gridy  = 2;
        gbc2.gridwidth = 2;
        
        gbc3.gridx  = 0;
        gbc3.gridy  = 3;
        gbc3.gridwidth = 2;
        
        gbc4.gridx  = 0;
        gbc4.gridy  = 4;
        gbc4.insets = new Insets( 0, 0, 0, 5 );
        
        gbc5.gridx  = 1;
        gbc5.gridy  = 4;
       
        add( lblAgencia,  gbc1 );
        add( txtAgencia,  gbc1 ); 
        add( lblConta,	  gbc2 );
        add( txtConta,	  gbc2 );
        add( lblSenha,	  gbc3 );
        add( txtSenha,	  gbc3 ); 
        add( btnAcessar,  gbc4 ); 
        add( btnCancelar, gbc5 );
        
        // ADICIONA FUNCAO A CADA BOTAO
        btnAcessar.addActionListener ( this );
        btnCancelar.addActionListener( this );
	}

	@Override
	public void actionPerformed( ActionEvent evento )
	{
		if( evento.getSource() == btnAcessar )
		{
			try 
			{
				acessar();
			} 
			catch (Exception e) 
			{				
				e.printStackTrace();
			}	
			
			dispose();
		}
		else if( evento.getSource() == btnCancelar )
		{
			dispose();
		}
	}
	
	public void acessar() throws Exception
	{
		// ATRIBUTOS
		Acesso acesso = new Acesso();
		
		try 
		{	
			if( txtAgencia.getText().trim().equals("") || txtConta.getText().trim().equals("") )
			{
				JOptionPane.showMessageDialog( null, linguagem.getString( "guiAcesso.erroTexto" ), linguagem.getString( "guiAcesso.erroTitulo" ), JOptionPane.ERROR_MESSAGE );
				System.exit( 1 );
			}
			
			// ARMAZENA O RETORNO DA FUNCAO
			conta.setUsuario( acesso.login( txtSenha.getText(), Integer.parseInt( txtAgencia.getText().trim() ), Integer.parseInt( txtConta.getText().trim() ) ) );
			
			// TRATA O ERRO
			if( conta.getUsuario() == null )
			{
				JOptionPane.showMessageDialog( null, linguagem.getString( "guiAcesso.erroTexto" ), linguagem.getString( "guiAcesso.erroTitulo" ), JOptionPane.ERROR_MESSAGE );
				System.exit( 1 );
			}
			
			// ADMINISTRADOR
			if( conta.getUsuario().getTipo() == 1 )
			{
				// INSTANCIA A INTERFACE PRINCIPAL
				GUIAdministrador objAdministrador = new GUIAdministrador();
				
				objAdministrador.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );			
				objAdministrador.setSize( 800, 600 );
				objAdministrador.setResizable( false );			
				objAdministrador.setVisible( true );
				objAdministrador.setLocationRelativeTo( null );
			}
			
			// USUARIO
			if( conta.getUsuario().getTipo() == 0 )
			{						
				// INSTANCIA A INTERFACE PRINCIPAL
				GUIPrincipal objPrincipal = new GUIPrincipal();
				
				objPrincipal.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );			
				objPrincipal.setSize( 800, 600 );
				objPrincipal.setResizable( false );			
				objPrincipal.setVisible( true );
				objPrincipal.setLocationRelativeTo( null );
			}
		}
		catch( Exception erro ) 
		{
			erro.printStackTrace();
			System.exit(1);
		}
	}
}