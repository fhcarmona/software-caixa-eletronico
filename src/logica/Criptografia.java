// AUTORES: BRUNO, FABIO E LEONARDO
// 
// DESCRICAO:
//		CLASSE RESPONSAVEL A TUDO RELACIONADO A CRIPTOGRAFIA

// PACOTE
package logica;

// BIBLIOTECA
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Criptografia 
{
	// METODO MD5
	public String md5( String entrada ) throws NoSuchAlgorithmException
	{
		MessageDigest mensagemMD5 = MessageDigest.getInstance("MD5");
		
		mensagemMD5.update(entrada.getBytes(), 0, entrada.length());
		
		return new BigInteger( 1, mensagemMD5.digest() ).toString( 16 );
	}
}