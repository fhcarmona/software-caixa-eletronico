// AUTORES: BRUNO, FABIO E LEONARDO
// 
// DESCRICAO:
//		CLASSE RESPONSAVEL PELO MODELO USUARIO

// PACOTE
package logica;

public class Usuario 
{ 
	// ATRIBUTOS
	private String nome;
	private String senha;
	private int    agencia; 
	private int    conta; 
	private int    tipo;
	private int    token;
	
	// CONSTRUTOR SEM PARAMETROS
	public Usuario(){} 

	// CONSTRUTOR COM PARAMETROS	
	public Usuario( String nome, String senha, int agencia, int conta, int token, int tipo )
	{
		setNome   ( nome    ); 
		setSenha  ( senha	); 		
		setAgencia( agencia );
		setConta  ( conta   ); 
		setToken  ( token   );
		setTipo   ( tipo	);
	}

	// SET'S
	public void setNome   ( String nome    ){ this.nome    = nome;    }
	public void setSenha  ( String senha   ){ this.senha   = senha;	  }
	public void setAgencia( int    agencia ){ this.agencia = agencia; }
	public void setConta  ( int    conta   ){ this.conta   = conta;	  }	
	public void setToken  ( int	   token   ){ this.token   = token;	  }
	public void setTipo	  ( int	   tipo	   ){ this.tipo    = tipo; 	  }
	
	// GET'S
	public String getNome()   { return this.nome;    }
	public String getSenha()  { return this.senha;	 }
	public int getAgencia()   { return this.agencia; }
	public int getConta()     { return this.conta;	 }	
	public int getToken()	  { return this.token;	 }
	public int getTipo()	  { return this.tipo;	 }
}