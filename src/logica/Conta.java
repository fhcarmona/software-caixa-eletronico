// AUTORES: BRUNO, FABIO E LEONARDO
// 
// DESCRICAO:
//		CLASSE SINGLETON, ARMAZENA AS CONFIGURACOES QUE SERAO NECESSARIAS PARA O PROGRAMA

// PACOTE
package logica;

// BIBLIOTECA
import java.util.ResourceBundle;

public class Conta
{
	// INSTANCIA DA CLASSE
    private static Conta		  instancia;
    private static ResourceBundle linguagem;
    private static Usuario		  usuario;
    private static int 			  tentativas = 3;
    
    private static Criptografia	  objCriptografia;

    // GET
    public static Conta getInstancia()
    {
        if( instancia == null )
        	instancia = new Conta();
        
        return instancia;
    }
    
    public static ResourceBundle getLinguagem()
    {       
        return linguagem;
    }
    
    public static Usuario getUsuario()
    {
        return usuario;
    }    
    
    public static int getTentativas()
    {
    	return tentativas;
    }
    
    public static Criptografia getCriptografia()
    {
    	return objCriptografia;
    }
    
    // SET
    public static void setCriptografia()
    {
        objCriptografia = new Criptografia();
    }
    
    public static void setLinguagem( ResourceBundle idioma )
    {
    	linguagem = idioma;
    }
    
    public static void setUsuario( Usuario user )
    {
    	usuario = user;
    }
    
    public static void erroTentativa()
    {
    	tentativas--;
    }
    
    public static void resetaTentativas()
    {
    	tentativas = 3;
    }
}
