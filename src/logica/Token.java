// AUTORES: BRUNO, FABIO E LEONARDO
// 
// DESCRICAO:
//		CLASSE RESPONSAVEL PELA VERIFICACAO DO TOKEN, MELHOR CLASSE

package logica;

public class Token
{
	private boolean primeiroPar = false;
	private boolean segundoPar  = false;
	private boolean terceiroPar = false;
	
	private Conta   conta 		= Conta.getInstancia();
	
	// GERA OS VALORES DOS CAMPOS DO TOKEN
	public String[] geraValoresToken( int token )
	{
		// ATRIBUTO COM OS VALORES DO CAMPO
		String[] retorno = new String[6];
		
		int      linha    = (int)( Math.random() * 6 );	;
		int      tokenPar = -1;
		int      errado[] = new int[6];
		
		// PRIMEIRO PAR DO TOKEN
		if( !primeiroPar )
		{			
			// DEFINE QUE FOI EXECUTADO A AVALIACAO DO PRIMEIRO PAR
			this.primeiroPar = true;
			
			// SEPARA O PRIMEIRO PAR DO TOKEN
			tokenPar = Integer.parseInt( Integer.toString( token ).substring( 0, 2 ) );
		}
		else if( !segundoPar )
		{
			// DEFINE QUE FOI EXECUTADO A AVALIACAO DO PRIMEIRO PAR
			this.segundoPar = true;

			// SEPARA O PRIMEIRO PAR DO TOKEN
			tokenPar = Integer.parseInt( Integer.toString( token ).substring( 2, 4 ) );			
		}
		else if( !terceiroPar )
		{
			// DEFINE QUE FOI EXECUTADO A AVALIACAO DO PRIMEIRO PAR
			this.terceiroPar = true;

			// SEPARA O PRIMEIRO PAR DO TOKEN
			tokenPar = Integer.parseInt( Integer.toString( token ).substring( 4 ) );			
		}
		
		// ADICIONA O TOKEN A LINHA
		retorno[ linha ] = Integer.toString( tokenPar ); 
		
		// PREENCHE OS DEMAIS VALORES DO ARRAY
		retorno = preencheArray( retorno, linha );
			
		return retorno;
	}
	
	// GERA UM VALOR ALEATORIO, DIFERENTE DO VALOR DO PARAMETRO
	public int geraValorFalso( int tokenPar )
	{
		int valor = tokenPar;
		
		while( valor == tokenPar )
			valor = (int)( Math.random() * 90 ) + 10; 
		
		return valor;
	}
	
	// PREENCHE OS VALORES EM BRANCOS DE UM ARRAY POR NUMEROS ALEATORIOS, SEM REPETIR ELES
	public String[] preencheArray( String[] array, int linha )
	{
		// ATRIBUTOS
		int     incremento = 0;
		boolean preenchido = false;
		
		// PERCORRE O ARRAY ENQUANTO NAO FOR PREENCHIDO
		while( incremento<6 )
		{
			// VERIFICA OS CAMPOS DIFERENTE DA LINHA DO TOKEN
			if( incremento != linha )
			{
				// CALCULA O NUMERO ALEATORIO ENTRE 10-99
				array[ incremento ] = (int)( Math.random() * 90 ) + 10 + "";
				
				preenchido = true;
				
				// PERCORRE O ARRAY NA BUSCA DE NUMEROS IGUAIS
				for( int i=0; i<5; i++ )
				{	
					// RETIRA DA VERIFICACAO DA LINHA QUE FOI PREENCHIDA AGORA
					if( incremento == i )
						i++;
					
					// SE ENCONTRAR UM NUMERO IGUAL
					if( array[ incremento ] == array[ i ] )
						preenchido = false;
				}
			}
			else
				preenchido = true;

			if( preenchido )
				incremento++;
			
			preenchido = false;
		}
		
		return array;
	}
	
	// VALIDA O TOKEN
	public boolean validarToken( String texto )
	{
		if( texto.equals( Integer.toString( conta.getUsuario().getToken() ) ) )
			return true;
		
		return false;
	}
	
	// VERIFICA SE TODOS OS CAMPOS FORAM PREENCHIDOS
	public boolean verificaPreenchimento()
	{
		if( primeiroPar && segundoPar && terceiroPar )
			return true;
		
		return false;
	}
	
	public boolean retirarDinheiro( int valor )
	{
		Sacar objSacar = new Sacar();

		return objSacar.sacarValor( valor );
	}
}
