// AUTORES: BRUNO, FABIO E LEONARDO
// 
// DESCRICAO:
//		CLASSE RESPONSAVEL PELA INSERCAO DE DADOS NO LOG

// PACOTE
package logica;

// BIBLIOTECA
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import dao.BDConexao;
import dao.BDInsercao;
import dao.BDSelecao;

public class Log
{
	// ATRIBUTO
	private int	   codigo;
	private int    cliente;
	private int    conta;
	private int    movimentacao;
	
	private double valor;
	
	private BDConexao  objConexao;
	private BDSelecao  objSelecao;
	private BDInsercao objInsercao;
	
	private ResultSet  resultado;
	private Connection conexao;
	
	private DateFormat dataFormato = new SimpleDateFormat("YYYY-MM-dd");
	
	public Log( int codigo, double valor, String cliente, int conta, int movimentacao )
	{
		// INSTANCIA OS DAO
		this.objConexao	 = new BDConexao ();
		this.objSelecao  = new BDSelecao ();
		this.objInsercao = new BDInsercao();
		
		// CONEXAO
		try					   { this.conexao = objConexao.novaConexao(); } 
		catch( SQLException e ){ e.printStackTrace();  					  }
		
		// MODIFICA OS VALORES DOS ATRIBUTOS
		setCodigo	   ( codigo		  );
		setValor	   ( valor		  );
		setCliente	   ( cliente	  );
		setConta	   ( conta		  );
		setMovimentacao( movimentacao );
		
		// INSERE UMA LINHA NO LOG
		inserirLog();
	}
	
	// SET
	public void setCodigo( int    codigo ){ this.codigo = codigo; }
	public void setValor ( double valor  ){ this.valor  = valor;  }
	
	public void setCliente( String cliente )
	{
		// CONSULTA
		resultado = objSelecao.select("SELECT cliente.cliCodigo FROM cliente LEFT JOIN log ON log.cliCodigo = cliente.cliCodigo WHERE cliente.cliNome = '" + cliente + "'", this.conexao);

		try					 { while( resultado.next() ) this.cliente = resultado.getInt( 1 ); }
		catch(SQLException e){ e.printStackTrace();					 						   }
	}
	
	public void setConta( int conta )
	{
		// CONSULTA
		resultado = objSelecao.select("SELECT conta.conCodigo FROM conta LEFT JOIN log ON log.conCodigo = conta.conCodigo WHERE conta.conConta = " + conta, this.conexao);
		
		try					 { while( resultado.next() ) this.conta = resultado.getInt( 1 ); }
		catch(SQLException e){ e.printStackTrace();				   							 }
	}
	
	public void setMovimentacao( int movimentacao )
	{
		this.movimentacao = 0;
	}
	
	// GET
	public int	  getCodigo()	   { return this.codigo;	   }
	public double getValor()	   { return this.valor;		   }
	public int	  getCliente()	   { return this.cliente;	   }
	public int	  getConta()	   { return this.conta;		   }
	public int	  getMovimentacao(){ return this.movimentacao; }
	
	// OTHERS
	public void inserirLog()
	{	
		objInsercao.insert( "INSERT INTO log ( logTipo, logValor, logData, cliCodigo, conCodigo, movCodigo ) VALUES ( " + getCodigo() + ", " + getValor() + ", '" + dataFormato.format(new Date()) + "', " + getCliente() + ", " + getConta() + ", " + null + " )", conexao );
	}
}
