package logica;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import dao.BDConexao;
import dao.BDInsercao;
import dao.BDSelecao;

public class Sacar
{
	// ATRIBUTOS
	private BDConexao  objBDConexao  = new BDConexao();
	private BDInsercao objBDInsercao = new BDInsercao();
	private BDSelecao  objBDSelecao  = new BDSelecao();
	
	private Connection conexao;
	private ResultSet  resultado;
	
	private Conta      conta = Conta.getInstancia();
	private Log		   log;
	
	public boolean sacarValor( int valor )
	{
		double saldo   = 0;
		int    notas[] = contaDinheiro( valor );
			
		try
		{
			// ABRE A CONEXAO
			conexao = objBDConexao.novaConexao();
			
			// FORCA A NAO ATUALIZACAO NO BANCO
			conexao.setAutoCommit(false);
			
			// ARMAZENA O RESULTADO DA CONSULTA
			resultado = objBDSelecao.select( "SELECT conSaldo FROM conta WHERE conAgencia = " + conta.getUsuario().getAgencia() + " AND conConta = " + conta.getUsuario().getConta(), conexao );
			
			// RETORNA FALSE, SE NAO TIVER SALDO SUFICIENTE
			while( resultado.next() )
				saldo = resultado.getDouble( 1 );
			
			if( saldo < valor )
				return false;
			
			// FORCA A NAO ATUALIZACAO NO BANCO
			conexao.setAutoCommit(true);

			objBDInsercao.insert( "UPDATE conta SET conSaldo = " + ( saldo-valor ) + " WHERE conAgencia = " + conta.getUsuario().getAgencia() + " AND conConta = " + conta.getUsuario().getConta(), conexao );
			objBDInsercao.insert( "UPDATE dispenser SET disCinco = " + notas[0] + ", disDez = " + notas[1] + ", disVinte = " + notas[2] + ", disCinquenta = " + notas[3] + ", disCem = " + notas[4], conexao );
			
			log = new Log( 10, valor, conta.getUsuario().getNome(), conta.getUsuario().getConta(), 0 );
			
			conexao.close();
			
			return true;
		} 
		catch( SQLException erro )
		{
			erro.printStackTrace();
		}
		
		return false;
	}
	
	public int[] contaDinheiro( int valor )
	{
		// ATRIBUTOS
		int cinco	  = 0;
		int dez		  = 0;
		int vinte	  = 0;
		int cinquenta = 0;
		int cem		  = 0;
		
		try
		{
			// ABRE A CONEXAO
			conexao = objBDConexao.novaConexao();
			
			// FORCA A NAO ATUALIZACAO NO BANCO
			conexao.setAutoCommit(false);
			
			// ARMAZENA O RESULTADO DA CONSULTA
			resultado = objBDSelecao.select( "SELECT disCinco, disDez, disVinte, disCinquenta, disCem FROM dispenser", conexao );
			
			// ARMAZENA O RESULTADO DA CONSULTA
			while( resultado.next() )
			{
				cinco	  = resultado.getInt( 1 );
				dez		  = resultado.getInt( 2 );
				vinte	  = resultado.getInt( 3 );
				cinquenta = resultado.getInt( 4 );
				cem		  = resultado.getInt( 5 );
			}

			// CONTA A MENOR QUANTIDADE POSSIVEL DE NOTAS
			if( valor >= 100 ){ while( valor >= 100 && cem       > 0 ){ valor = valor-100; cem--;		} };
			if( valor >= 50  ){ while( valor >= 50  && cinquenta > 0 ){ valor = valor-50;  cinquenta--; } };
			if( valor >= 20  ){ while( valor >= 20  && vinte	 > 0 ){ valor = valor-20;  vinte--;		} };		
			if( valor >= 10  ){ while( valor >= 10  && dez	     > 0 ){ valor = valor-10;  dez--;		} };		
			if( valor >= 5   ){ while( valor >= 5   && cinco	 > 0 ){ valor = valor-5;   cinco--;		} };	
		}
		catch( SQLException erro )
		{
			erro.printStackTrace();
			System.exit( 1 );
		}
		
		// RETORNA A QUANTIDADE A SER RETIRADA
		return new int[]{ cinco, dez, vinte, cinquenta, cem };
	}	
}
