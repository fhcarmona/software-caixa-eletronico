// AUTORES: BRUNO, FABIO E LEONARDO
// 
// DESCRICAO:
//		CLASSE RESPONSAVEL PELAS ACOES RELACIONADAS AO ARQUIVO TXT

// PACOTE
package logica;

// BIBLIOTECA
import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.FormatterClosedException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import javax.swing.JOptionPane;

import dao.BDConexao;
import dao.BDSelecao;

public class ArquivoTXT
{
	// SCANNER
	private Formatter		   output;
	private Scanner 		   registro;
	private ArrayList<Usuario> listaUsuario = new ArrayList<Usuario>();
	
	private Criptografia	   objCripto = new Criptografia();
	
	// ABRE O ARQUIVO
    public void abrir()
    {
        try
        {
        	registro = new Scanner( new File( "acesso.txt" ) );
        }
        catch( FileNotFoundException erro )
        {
            System.exit(1);
        }
    }
 
    // RETORNA O CONTEUDO DO ARQUIVO COMO ARRAYLIST
    public ArrayList<Usuario> ler() throws Exception
    {
        try
        {
            while( registro.hasNext() )          
            {
            	Criptografia objCriptografia = new Criptografia();
            	
            	// ATRIBUTOS
            	String nome    = registro.next();
            	String senha   = registro.next();
            	
            	int    agencia = registro.nextInt();
            	int    conta   = registro.nextInt();
            	int    token   = registro.nextInt();
            	int    tipo    = registro.nextInt();
            	
            	Usuario objUsuario = new Usuario( nome, senha, agencia, conta, token, tipo );
            	
            	listaUsuario.add( objUsuario );
            }
        }
        catch( NoSuchElementException erro )
        {
            registro.close();
            System.exit(1);
        }
        catch( IllegalStateException erro )
        {
            System.exit(1);
        }
        
        return listaUsuario;
    }
 
    // FECHAR ARQUIVO
    public void fechar()
    {
        registro.close();
    }
    
	//  METODO PARA ADICIONAR REGISTROS NO ARQUIVO TXT	
	public void criarArquivoTXT() throws FileNotFoundException
	{
		// ATRIBUTOS
		ArrayList<Usuario> listaBD   = new ArrayList<Usuario>();
		Connection 		   conexao	 = null;
		ResultSet		   resultado = null;
		
		try
		{
			BDConexao objBDConexao = new BDConexao();
			BDSelecao objBDSelecao = new BDSelecao();
			
			conexao = objBDConexao.novaConexao();
			
			conexao.setAutoCommit(false);
			
			resultado = objBDSelecao.select( "SELECT cliente.cliNome, conSenha, conAgencia, conConta, conToken, conTipo  FROM conta INNER JOIN cliente ON cliente.cliCodigo = conta.cliCodigo ORDER BY conConta", conexao );
			
			while( resultado.next() )
			{				
				String nome    = resultado.getString( 1 );
				String senha   = resultado.getString( 2 );
				int    agencia = resultado.getInt( 3 );
				int    conta   = resultado.getInt( 4 );
				int    token   = resultado.getInt( 5 );
				int    tipo    = resultado.getInt( 6 );
				
				Usuario usuario = new Usuario( nome, senha, agencia, conta, token, tipo );
				
				listaBD.add( usuario );
			}
		}
		catch( Exception erro )
		{
			erro.printStackTrace();
			System.exit( 1 );
		}
		
		// CRIA O ARQUIVO DE ACESSO
		output = new Formatter( "acesso.txt" ); 
		
		// PERCORRE CADA LINHA DO ARRAYLIST
		for ( Usuario linha : listaBD ) 
		{ 
			try
			{						
				// ADICIONA O REGISTRO
				output.format( "%s %s %d %d %d %d\n", linha.getNome(), linha.getSenha(), linha.getAgencia(), linha.getConta(), linha.getToken(), linha.getTipo() );
			}
			catch( FormatterClosedException formatterClosedException ) 
			{  
				formatterClosedException.printStackTrace();
				System.exit( 1 );
			}
		}
		
		// FECHA O ARQUIVO
		output.close();
	}
	
	// GET
	public ArrayList<Usuario> getListaUsuario() throws Exception
	{
		abrir();
		ler();
		fechar();
		
		return this.listaUsuario;
	}
}
