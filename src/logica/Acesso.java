// AUTORES: BRUNO, FABIO E LEONARDO
// 
// DESCRICAO:
//		CLASSE RESPONSAVEL PELO LOGICA DE LOGIN E BUSCA BINARIA

// PACOTE
package logica;

// BIBLIOTECA
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.FormatterClosedException;
import java.util.Scanner;
import java.lang.IllegalStateException;
import java.nio.charset.Charset;
import java.util.NoSuchElementException;
import java.io.FileNotFoundException;

import dao.BDConexao;
import dao.BDSelecao;

import javax.swing.JOptionPane;

import logica.Criptografia;

public class Acesso
{
	private ArquivoTXT		   objArquivo   = new ArquivoTXT();
	private ArrayList<Usuario> listaUsuario;
	
	public Acesso() throws Exception
	{
		this.listaUsuario = objArquivo.getListaUsuario();
	}
	
	// RETORNA A PERMISSAO DE ACESSO
 	public Usuario login( String senha, int agenciaNumero, int contaNumero )
 	{
 		// ATRIBUTOS		
 		int linha = -1;
 		Criptografia objCriptografia = new Criptografia();
 		
 		try
 		{
 			// ARMAZENA OS DADOS DO TXT EM UM ARRAYLIST
 			objArquivo.abrir();
 			objArquivo.ler();
 			objArquivo.fechar();
 			
 			// ENCONTRA LINHA ONDE ESTA LOCALIZADO A CONTA
 			linha = buscarConta( contaNumero ); 
 			
 			// SE FOI ENCONTRADO UMA LINHA COM A CONTA
 			if( linha != -1 )
 			{ 				
 				// VERIFICA TODOS OS DADOS DO USUARIO
 				if( listaUsuario.get( linha ).getSenha().equals( objCriptografia.md5( senha ) ) && listaUsuario.get( linha ).getConta() == contaNumero && listaUsuario.get( linha ).getAgencia() == agenciaNumero )
 				{
 					String nome	 = listaUsuario.get( linha ).getNome();
 					int    token = listaUsuario.get( linha ).getToken();
 					int    tipo  = listaUsuario.get( linha ).getTipo();
 					
 					Usuario user = new Usuario( nome, senha, agenciaNumero, contaNumero, token, tipo );
 					
 					return user;
 				}
 			}
 		}
 		catch( Exception erro )
 		{
 			erro.printStackTrace();
 		} 		
 		
 		// SEM PERMISSAO
 		return null;
 	}
 	
 	// BUSCA BINARIA
  	public int buscarConta( int conta )
  	{
  		// ATRIBUTOS
 		int inicio = 0;
 		int fim	   = listaUsuario.size() - 1;
 		
 		// PERCORRE O ARRAY
 		while( inicio <= fim )
 		{
 			// ARMAZENA O MEIO DO ARRAYLIST
 			int meio = ( inicio + fim ) / 2;
 			
 			// RETORNA SE O VALOR FOR ENCONTRADO
 			if( listaUsuario.get( meio ).getConta() == conta )
 				return meio;
 			
 			// VERIFICA SE A CONTA A SER ENCONTRADA E MAIOR OU MENOR
 			if( conta > listaUsuario.get(meio).getConta() )
 				inicio = meio + 1;
 			else
 				fim = meio - 1;
 		}
 		
 		// RETORNA SE NAO RETORNAR A CONTA
 		return -1;
 	}
}