// AUTORES: BRUNO, FABIO E LEONARDO
// 
// DESCRICAO:
//		CLASSE RESPONSAVEL POR ESCREVER OS RELATORIOS

// PACOTE
package logica;

// BIBLIOTECA
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import dao.BDConexao;
import dao.BDSelecao;

public class Relatorios
{
	// ATRIBUTOS
	private Connection	   conexao		= null;
	private ResultSet	   resultado	= null;		
	
	private BDConexao	   objBDConexao = new BDConexao();
	private BDSelecao	   objBDSelecao = new BDSelecao();
	
	private String		   texto		= "";
	
	private Conta		   conta		= Conta.getInstancia();
	private Log			   log;

	private ResourceBundle linguagem	= Conta.getLinguagem();
	
	// METODO PRINCIPAL
	public String escreverRelatorio( int tipo, String dataInicio, String dataFinal )
	{		
		if	   ( tipo == 1 )
			return relatorioExtrato( dataInicio, dataFinal );	
		else if( tipo == 2 )
			return relatorioSaldo();
		else if( tipo == 3 )
			return relatorioOperacoes();
		else if( tipo == 4 )
			return relatorioDispenser();
		else
			return "";
	}
		
	// LOGICA DOS RELATORIOS
	public String relatorioExtrato(String dataInicio, String dataFinal)
	{
		String texto = "";
		
		try
		{
			// ABRE A CONEXAO
			conexao = objBDConexao.novaConexao();
			
			// FORCA A NAO ATUALIZACAO NO BANCO
			conexao.setAutoCommit(false);
			
			// ARMAZENA O RESULTADO DA CONSULTA
			resultado = objBDSelecao.select( "SELECT (SELECT CASE WHEN logTipo = 10 THEN 'saque' WHEN logTipo = 11 THEN 'transferencia' END) AS lotTipo, logValor, logData, conta.conConta FROM log INNER JOIN conta ON conta.conCodigo = log.conCodigo WHERE logData BETWEEN '" + dataInicio + "' AND '" + dataFinal + "' AND conta.conConta = " + conta.getUsuario().getConta() + " AND logTipo <= 11", conexao );
			
			// ESCREVE O TEXTO
			texto += "\t\t\t" + linguagem.getString( "relatorioExtrato.titulo" ) + "\n\n";
			texto += "===================================================================================\n\n";
			texto += "\t\t" + linguagem.getString( "relatorioExtrato.tipo" ) + "\t" + linguagem.getString( "relatorioExtrato.valor" ) + "\t" + linguagem.getString( "relatorioExtrato.data" ) + "\n";
			
			while( resultado.next() )
			{					
				texto += "\t\t" + resultado.getString( 1 ) + "\tR$ " + resultado.getDouble( 2 ) + "\t" + resultado.getString( 3 ) + "\n";
			}
			
			texto += "\n\n===================================================================================";
			
			log = new Log( 21, 0, conta.getUsuario().getNome(), conta.getUsuario().getConta(), 0 );
			
			conexao.close();
		} 
		catch( SQLException erro )
		{
			erro.printStackTrace();
		}
		 
		return texto;
	}
	
	public String relatorioSaldo()
	{
		String texto = "";
		
		try
		{
			// ABRE A CONEXAO
			conexao = objBDConexao.novaConexao();
			
			// FORCA A NAO ATUALIZACAO NO BANCO
			conexao.setAutoCommit(false);
			
			// ARMAZENA O RESULTADO DA CONSULTA
			resultado = objBDSelecao.select( "SELECT conSaldo FROM conta WHERE conAgencia = " + conta.getUsuario().getAgencia() + " AND conConta = " + conta.getUsuario().getConta(), conexao );
			
			// ESCREVE O TEXTO
			texto += "\t\t\t" + linguagem.getString( "relatorioSaldo.titulo" ) + "\n\n";
			texto += "===================================================================================\n\n";
			
			while( resultado.next() )
			{
				texto += linguagem.getString( "relatorioSaldo.saldoDisponivel" ) + ":\n";
				texto += "\tR$ " + resultado.getDouble( 1 ) + "\n";
				texto += linguagem.getString( "relatorioSaldo.saldoBloqueado" ) + ":\n";
				texto += "\tR$ 00.0\n\n\n\n\n\n\n\n\n\n";
			}
			
			texto += "===================================================================================";
			
			log = new Log( 20, 0, conta.getUsuario().getNome(), conta.getUsuario().getConta(), 0 );
		}
		catch( SQLException erro )
		{
			erro.printStackTrace();
		}
		
		return texto;
	}
	
	public String relatorioOperacoes()
	{
		String texto = "";
		
		try
		{
			// ABRE A CONEXAO
			conexao = objBDConexao.novaConexao();
			
			// FORCA A NAO ATUALIZACAO NO BANCO
			conexao.setAutoCommit(false);
			
			// ARMAZENA O RESULTADO DA CONSULTA
			resultado = objBDSelecao.select( "SELECT (SELECT CASE WHEN logTipo = 10 THEN 'SAQUE' WHEN logTipo = 11 THEN 'TRANSFERENCIA' WHEN logTipo = 20 THEN 'SALDO' WHEN logTipo = 21 THEN 'EXTRATO' WHEN logTipo = 30 THEN 'DEBITO' END) AS lotTipo, logValor, logData, conta. conConta FROM log INNER JOIN conta ON conta.conCodigo = log.conCodigo ORDER BY logData", conexao );
			
			// ESCREVE O TEXTO
			texto += "\t\t\t" + linguagem.getString( "relatorioOperacoes.titulo" ) + "\n\n";
			texto += "===================================================================================\n\n";
			texto += "\t" + linguagem.getString( "relatorioOperacoes.tipo" ) + "\t\t" + linguagem.getString( "relatorioOperacoes.valor" ) + "\t" + linguagem.getString( "relatorioOperacoes.data" ) + "\t" + linguagem.getString( "relatorioOperacoes.conta" ) + "\n\n";
			
			while( resultado.next() )
			{					
				// MUDA APENAS A FORMATACAO
				if( resultado.getString( 1 ).equals( "TRANSFERENCIA" ) )
					texto += "\t" + resultado.getString( 1 ) + "\tR$ " + resultado.getDouble( 2 ) + "\t" + resultado.getString( 3 ) + "\t" + resultado.getInt( 4 ) + "\n";
				else
					texto += "\t" + resultado.getString( 1 ) + "\t\tR$ " + resultado.getDouble( 2 ) + "\t" + resultado.getString( 3 ) + "\t" + resultado.getInt( 4 ) + "\n";
			}
			
			texto += "\n\n===================================================================================";
		}
		catch( SQLException erro )
		{
			erro.printStackTrace();
		}
		
		return texto;
	}
	
	public String relatorioDispenser()
	{
		String texto = "";
		
		try
		{
			// ABRE A CONEXAO
			conexao = objBDConexao.novaConexao();
			
			// FORCA A NAO ATUALIZACAO NO BANCO
			conexao.setAutoCommit(false);
						
			// ARMAZENA O RESULTADO DA CONSULTA
			resultado = objBDSelecao.select( "SELECT disCinco, disDez, disVinte, disCinquenta, disCem FROM dispenser", conexao );
			
			// ESCREVE O TEXTO
			texto += "\t\t\t" + linguagem.getString( "relatorioDispenser.titulo" ) + "\n\n";
			texto += "===================================================================================\n\n";
			texto += "\t" + linguagem.getString( "relatorioDispenser.cinco" ) + "\t" + linguagem.getString( "relatorioDispenser.dez" ) + "\t" + linguagem.getString( "relatorioDispenser.vinte" ) + "\t" + linguagem.getString( "relatorioDispenser.cinquenta" ) + "\t" + linguagem.getString( "relatorioDispenser.cem" ) + "\n\n";
			
			while( resultado.next() )
			{
				
				texto += "\t" + resultado.getInt( 1 ) + "U\t" + resultado.getInt( 2 ) + "U\t" + resultado.getInt( 3 ) + "U\t" + resultado.getInt( 4 ) + "U\t" + resultado.getInt( 5 ) + "U\n\n\n\n\n\n\n\n\n\n\n\n";
			}
			
			texto += "===================================================================================";
		}
		catch( SQLException erro )
		{
			erro.printStackTrace();
		}
		
		return texto;
	}
}
